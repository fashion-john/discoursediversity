# Discourse Diversity

A Python implementation inspired by the methodology to measure discourse bias proposed [Dmitry Paranyushkin on Towards Datascience blog](https://towardsdatascience.com/measuring-discourse-bias-using-text-network-analysis-9f251be5f6f3) blog. 

The approach is based on converting a text into network and identifying its community structure and modularity score. The most influential nodes are identified based on betweenness centrality measure and the entropy of their distribution among the communities is then estimated. As a result, we obtain a representation of the different topical contexts present within the text and how well they are interconnected. The basic assumption is that for the texts with a high level of modularity, pronounced community structure, and the high entropy of disribution among the communities for the top BC nodes the level of discourse diversity is high. If the community structure within the text is low (there are no distinct topics) and / or the entropy of the top BC nodes distributions is high (the most influential keywords belong to one topic), the level of diversity is low and therefore we assume that the text is biased towards a certain topic.

We use a modified version of the network analysis approach taken from [Identifying the Pathways for Meaning Circulation using Text Network Analysis, Paranyushkin, 2011/2012](https://noduslabs.com/publications/Pathways-Meaning-Text-Network-Analysis.pdf), adjusted to have the results and network metrics consistent with the current version of [InfraNodus Text Network Analysis Tool](http://github.com/noduslabs/infranodus). [NLTK default Porter stemmer](https://www.nltk.org/_modules/nltk/stem/porter.html) is used instead of Kstem. For advanced users, [pyhunspell](https://github.com/blatinier/pyhunspell) is also available for stemming, which is more similar to the one used in InfraNodus. This feature is to be considered experimental due to complex setup process and lack of proper testing for different platforms.

Google Scholar holds a full list of [papers by Dmitry Paranyushkin](https://scholar.google.com/citations?user=7fyleFwAAAAJ&hl=en).

This library is intended to be run in a Jupyter Notebook. Any error or info messages are therefore printed out
rather than sent to a log file or stdout.

# Get started

Clone or download this repository to you local machine and CD into it in the terminal.

If you clone the repository:
```
git clone <SSH or HTTPS URL to this repository>
cd discoursediversity
```

If you download the repository, rember to uncompress it and move it to a good location on your computer. Then cd into it:
```
cd discoursediversity-master
```

Supposing you have downloaded and installed [miniconda](https://conda.io/miniconda.html) or the full size [Anaconda Python (3.7.1)](https://www.anaconda.com/download/#macos), navigate to the project folder and run in terminal:
```
conda env create -f environment.yaml
```
Yo can read more about managing [environments in Conda and creating them from a file here](https://conda.io/docs/user-guide/tasks/manage-environments.html#create-env-from-file).
If Conda complains about the requirement `discoursediversity`, you can just ignore it.

To activate the environment and have access to all the importable libraries from your code, run
```
source activate discoursediversity
```

If there are errors you might need to install additional libraries required, such as
```
pip install python-louvain==0.13
```

To deactivate the environment for access to other environments or you default system environment run
```
source deactivate
```
You might also need to download the corpora used by textblob with the following command in the terminal

```
python -m textblob.download_corpora
```

Then, still in the project folder, start the Jupyter Notebook server by running

```
jupyter notebook
```

Now your browser should have opened a web view which allows you to edit and run the code in the notebooks. 

If you have any problems while executing the code, you might have to get the Porter stemmer activated. 

If you are on the `usage_example` notebook, you can simply change the line `experiment = did.TextNetwork(text, fname)` to `experiment = did.TextNetwork(text, fname, stemmer="porter")`

In case you want to create your own notebook, the full code to read a file for comparison is:

```
import discoursediversity as did
fileobj = open("texts/en/existential_terrains.txt")
fname = fileobj.name
text = fileobj.read()
experiment = did.TextNetwork(text, fname, stemmer="porter")

data = experiment.run()
```

[![Quick introduction to Jupyter Notebook](http://img.youtube.com/vi/jZ952vChhuI/0.jpg)](http://www.youtube.com/watch?v=jZ952vChhuI "Quick introduction to Jupyter Notebook")

# Usage
```
import discoursediversity as did

text = "En väldigt lång text på svenska, med massor av meningar och ord staplade på varandra."
t = did.Text(text)
data = t.run()
```

More example of how to use discoursediversity (and Jupyter Notebooks) is found in the tutorial notebook **example_usage.ipynb**.

# Evaluation

This code was evaluated on the [Hyperpartisan News Detection PAN @ SemEval 2019](https://pan.webis.de/semeval19/semeval19-web/) dataset. The results showed that there is no correlation between the discourse diversity level and the "ground truth" hyperpartisan score given to every article. In other words, both "right" and "left" news articles have shown more or less a similar level of discourse diversity. The code with instructions to get a copy of the dataset is found in the folder named `evaluation_PAN@SemEval_2019` in this repository. 

This code was also evaluated on [Wikipedia NPOV articles](https://en.wikipedia.org/wiki/Category:NPOV_disputes_from_April_2019), which list the articles that are considered to have disputes regarding the neutrality (70 articles). We compared them with the so-called ["good" articles on Wikipedia](https://en.wikipedia.org/wiki/Wikipedia:Good_articles/all) (70 randomly chosen ones). We found that only about 25% were marked as "Focused", "Biased" and "Polarized" by the algorithm and there was a higher proportion of those in the "good" articles. Also the results did not correlate with the `NPOV` measure. You can see the results in the folder `evaluation_WikipediaNPOV` in this repository. We are now studying whether the articles marked as "Polarized" or "Focused" have any specific properties that make them different from the rest. We will also run more tests on different datasets.

If you would like to scrape more data from Wikipedia / different data, you can use the [Nodus Labs scraper](https://github.com/deemeetree/datascrape) to get your own datasets to try (it also has the XML files from the test above).

Further evaluation with different datasets is needed and appreciated. 



