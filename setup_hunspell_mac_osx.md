First make shure you have hunspell installed on your computer.

On Mac OS X the installation is very complicated. I got it to work the following way, but honestly I believe you should use the NLTK PorterStemmer instead by passing stemmer="porter" to the TextNetwork class if you run into trouble with hunspell. That way the results will diverge from the ones obtained at infranodus.com quite a bit.

```
brew install hunspell
ln -s /usr/local/Cellar/hunspell/1.7.0_1/lib/libhunspell-1.7.0.dylib /usr/local/Cellar/hunspell/1.7.0_1/lib/libhunspell.dylib
```

# Download pyhunspell
```
cd pyhunspell-master
```
# Change the version numbers in the path in setup.py

    elif platform.system() == "Darwin":
        ...
        main_module_kwargs['include_dirs'] = '/usr/local/Cellar/hunspell/1.7.0_1/include/hunspell', 
        ...
```
CFLAGS=$(pkg-config --cflags hunspell) LDFLAGS=$(pkg-config --libs hunspell) pip install hunspell
``

Download .dic + .aff files for each language e.g. sv_SE.dic amd sv_SE.aff from e.g. https://github.com/elastic/hunspell/tree/master/dicts

Dictionaries should be placed in /Library/Spelling/

Further instructions can be found at https://github.com/hunspell/hunspell#usage

check if they are available to hunspell by running in the terminal

```
hunspell 
```